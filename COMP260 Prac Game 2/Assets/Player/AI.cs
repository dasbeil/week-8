﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class AI : MonoBehaviour {

	private Rigidbody rigidbody;
	public float speed = 1f;
	public Transform puck;
	public bool reset = false;
	public LayerMask puckLayer;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
		Defence ();
	}
	public void Defence() {
		Vector2 direction = puck.position - transform.position;
		Vector2 home;
		home.x = 6;
		home.y = 0;
		Vector2 PuckPosition = puck.position;
		if (PuckPosition.x > 1) {
			if (!reset) {
				rigidbody.velocity = direction * speed;
			} else {
				if (transform.position.x <= 5.48) {
					rigidbody.velocity = home;
				} else if (transform.position.x >= 5.5) {
					reset = false;
					Debug.Log ("HI");
				}
			}
		} else {
			if (transform.position.x <= home.x) {
				rigidbody.velocity = home;
			}
		}
	}
	void OnCollisionEnter(Collision collision) {
		if (puckLayer.Contains(collision.gameObject)) {
			reset = true;
			Debug.Log ("BYE");
		}
	}

}
